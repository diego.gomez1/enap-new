import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

const VideoBlock = ({ video, title }) => {
    return (
        <div>
            <div className="flex gap-4 align-middle items-center min-h-[3em] pb-2">
                <div className="text-xl">
                    <i className="fa fa-youtube-play"></i>
                </div>
                <div className="text-xs">
                    {title.toUpperCase()}
                </div>
            </div>
            <div className="w-full bg-gray-300 h-0 relative aspect-w-16 aspect-h-9">
                <iframe className="w-full h-full absolute top-0 left-0 right-0 bottom-0" width="560" height="315" src={video} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
            </div>
        </div>
    )
};

const ButtonLink = ({ link, children }) => {
    return (
        <div className="min-h-[2em]">
            <a className="px-3 py-2 bg-gray-400 hover:bg-gray-500 text-white " href={link} target="_blank"> {children} </a>
        </div>
    )
}

const LastYears = ({ }) => {

    let tabClass = "px-4 py-3 bg-gray-200 cursor-pointer";
    let tabPanelClass = "min-h-[12em] bg-white p-4 py-8";
    let gridVideosClass = "text-center grid sm:grid-cols-2 md:grid-cols-4 gap-2 overflow-hidden";
    let firstColumnClass = "flex flex-col gap-2 h-full justify-around";

    return (
        <Tabs className="mt-8">
            <TabList className="flex flex-1 flex-row gap-1 max-w-full overflow-hidden overflow-x-auto">
                <Tab className={tabClass}>2021</Tab>
                <Tab className={tabClass}>2020</Tab>
                <Tab className={tabClass}>2019</Tab>
                <Tab className={tabClass}>2018</Tab>
                <Tab className={tabClass}>2017</Tab>
                <Tab className={tabClass}>2016</Tab>
                <Tab className={tabClass}>2015</Tab>
                <Tab className={tabClass}>2014</Tab>
                <Tab className={tabClass}>2013</Tab>
                <Tab className={tabClass}>2012</Tab>                
            </TabList>
            <TabPanel>
                <div className={tabPanelClass}>
                    <div className={gridVideosClass}>
                        <div className={firstColumnClass}>
                            <h4>Espacios y Tiempos con Otra Mirada</h4>
                            <ButtonLink link="/pdf/programa_enap_2021.pdf">Programa 2021 PDF</ButtonLink>                            
                        </div>
                        <VideoBlock video="https://www.youtube.com/embed/nfsdR8VSU1E" title="PONENCIA DESTACADA - Valerio Rocco" />
                        <VideoBlock video="https://www.youtube.com/embed/en9HmTWTb8E" title="PONENCIA DESTACADA - Rafael Lamata" />                        
                        <VideoBlock video="https://www.youtube.com/embed/6PzchPRK4oo" title="RESUMEN DEL CONGRESO" />
                    </div>
                </div>
            </TabPanel>
            <TabPanel>
                <div className={tabPanelClass}>
                    <div className="text-center prose-sm">
                        <p>En el 2020, el IX congreso ENAP, no pudo ser por el advenimiento pandémico.</p>
                        <p>Lo planeamos, lo aplazamos, lo imaginamos, lo decidimos hacer presencial y como no fue posible…, nos resistimos a decir que no lo hicimos…, porque reflexionamos, sufrimos, flexibilizamos, entendimos que habíamos cubierto una etapa, un ciclo, …por ello queremos dejar ese hueco, simplemente ENAP 2020 existió, (vaya si existió)...pero se llamó “No nos vimos”.</p>
                    </div>
                </div>
            </TabPanel>
            <TabPanel>
                <div className={tabPanelClass}>
                    <div className={gridVideosClass}>
                        <div className={firstColumnClass}>
                            <h4>La evaluación en el siglo XXI</h4>
                            <ButtonLink link="reflexion2019">Reflexiones 2019</ButtonLink>
                            <ButtonLink link="/pdf/programa_enap_2019.pdf">Programa 2019 PDF</ButtonLink>
                            <ButtonLink link="https://www.youtube.com/watch?v=7PPS87E2lJ4&amp;list=PLtDwr4xyHdrx04k4GAKvraI506NB9DghL">Vídeos 2019</ButtonLink>
                        </div>
                        <VideoBlock video="https://www.youtube.com/embed/kfdwcQE3yac" title="PONENCIA DESTACADA - Jaime Buhigas" />
                        <VideoBlock video="https://www.youtube.com/embed/RQC6eqYcLmU" title="PONENCIA DESTACADA - Víctor Mourelle" />
                        <VideoBlock video="https://www.youtube.com/embed/7PPS87E2lJ4" title="RESUMEN DEL CONGRESO" />
                    </div>
                </div>
            </TabPanel>
            <TabPanel>
                <div className={tabPanelClass}>
                    <div className={gridVideosClass}>
                        <div className={firstColumnClass}>
                            <h4>La escuela está cambiando</h4>
                            <ButtonLink link="/pdf/programa_enap_2018.pdf">Programa 2019 PDF</ButtonLink>
                            <ButtonLink link="https://www.youtube.com/watch?v=kKX5accMoSg&amp;list=PLtDwr4xyHdrwlmkqatoWi5-NvvLBwLymD">Vídeos 2018</ButtonLink>
                        </div>
                        <VideoBlock video="https://www.youtube.com/embed/OozhpzsYB9E" title="PONENCIA DESTACADA - Jaime Buhigas" />
                        <VideoBlock video="https://www.youtube.com/embed/Ddd24son_Y0" title="PONENCIA DESTACADA - ELAINE GALLAGHER" />
                        <VideoBlock video="https://www.youtube.com/embed/UcCk7R-XOz0" title="RESUMEN DEL CONGRESO" />
                    </div>
                </div>
            </TabPanel>
            <TabPanel>
                <div className={tabPanelClass}>
                    <div className={gridVideosClass}>
                        <div className={firstColumnClass}>
                            <h4>El verdadero sentido de la orientación escolar</h4>
                            <ButtonLink link="/pdf/programa_enap_2017.pdf">Programa 2017 PDF</ButtonLink>
                            <ButtonLink link="https://www.youtube.com/playlist?list=PLtDwr4xyHdryFOVpzDV8aFzZA0ZrW5gXc">Vídeos 2017</ButtonLink>
                        </div>
                        <VideoBlock video="https://www.youtube.com/embed/AZNoVYGUcYs" title="PONENCIA DESTACADA - LAURA MARI BARRAJÓN" />
                        <VideoBlock video="https://www.youtube.com/embed/pR4c9Rg8qBs" title="PONENCIA DESTACADA - JAIME BUHIGAS" />
                        <VideoBlock video="https://www.youtube.com/embed/AOOc5gbZe-g" title="RESUMEN DEL CONGRESO" />
                    </div>
                </div>
            </TabPanel>
            <TabPanel>
                <div className={tabPanelClass}>
                    <div className={gridVideosClass}>
                        <div className={firstColumnClass}>
                            <h4>TransformARTE: el arte de vivir la educación</h4>
                            <ButtonLink link="/pdf/programa_enap_2016.pdf">Programa 2016 PDF</ButtonLink>
                            <ButtonLink link="https://www.youtube.com/playlist?list=PLtDwr4xyHdrz69uNCKb8MTaRBKmJ2TF1J">Vídeos 2016</ButtonLink>
                        </div>
                        <VideoBlock video="https://www.youtube.com/embed/W6VOBjlxXa0" title="PONENCIA DESTACADA - ALBERT BOADELLA" />
                        <VideoBlock video="https://www.youtube.com/embed/vlXKskcKJr8" title="PONENCIA DESTACADA - JAIME BUHIGAS" />
                        <VideoBlock video="https://www.youtube.com/embed/0uyBK8EJylI" title="RESUMEN DEL CONGRESO" />
                    </div>

                </div>
            </TabPanel>
            <TabPanel>
                <div className={tabPanelClass}>
                    <div className={gridVideosClass}>
                        <div className={firstColumnClass}>
                            <h4>Didáctica de la diversidad en el siglo XXI</h4>
                            <ButtonLink link="/pdf/programa_enap_2015.pdf">Programa 2015 PDF</ButtonLink>
                            <ButtonLink link="https://www.youtube.com/playlist?list=PLtDwr4xyHdrzJdy5VjKhhRx32t6zAu826">Vídeos 2015</ButtonLink>
                        </div>
                        <VideoBlock video="https://www.youtube.com/embed/1gJG8bcCF2g" title="PONENCIA DESTACADA - FRANCESC TORRALBA" />
                        <VideoBlock video="https://www.youtube.com/embed/0qGCiHXeZdI" title="PONENCIA DESTACADA - JOSÉ A. FERNÁNDEZ BRAVO" />
                        <VideoBlock video="https://www.youtube.com/embed/rHKaDWpLEx8" title="RESUMEN DEL CONGRESO" />
                    </div>
                </div>
            </TabPanel>
            <TabPanel>
                <div className={tabPanelClass}>
                    <div className={gridVideosClass}>
                        <div className={firstColumnClass}>
                            <h4>Sin emoción no hay aprendizaje: educando la emoción</h4>
                            <ButtonLink link="/pdf/programa_enap_2014.pdf">Programa 2014 PDF</ButtonLink>
                            <ButtonLink link="https://www.youtube.com/playlist?list=PLtDwr4xyHdrwQNSjz9VdPQuBVejI1XcD2">Vídeos 2014</ButtonLink>
                        </div>
                        <VideoBlock video="https://www.youtube.com/embed/avIQ1-EwILk" title="PONENCIA DESTACADA - PEPU HERNÁNDEZ" />
                        <VideoBlock video="https://www.youtube.com/embed/l4ClAPlZYOA" title="PONENCIA DESTACADA - LAURA MARI BARRAJÓN" />
                        <VideoBlock video="https://www.youtube.com/embed/Vp8cev4G6aw" title="RESUMEN DEL CONGRESO" />
                    </div>
                </div>
            </TabPanel>
            <TabPanel>
                <div className={tabPanelClass}>
                    <div className={gridVideosClass}>
                        <div className={firstColumnClass}>
                            <h4>Enseñar a pensar: educadores y filósofos</h4>
                            <ButtonLink link="/pdf/programa_enap_2013.pdf">Programa 2013 PDF</ButtonLink>
                            <ButtonLink link="https://www.youtube.com/playlist?list=PLtDwr4xyHdrxdFyd3f-sGmcHpwZooZ137">Vídeos 2013</ButtonLink>
                        </div>
                        <VideoBlock video="https://www.youtube.com/embed/G1lgICxuKlU" title="PONENCIA DESTACADA - DAVID PERKINS" />
                        <VideoBlock video="https://www.youtube.com/embed/50c9rzrLDZk" title="PONENCIA DESTACADA - ANGEL GABILONDO" />
                        <VideoBlock video="https://www.youtube.com/embed/-XO52h-_Za0" title="RESUMEN DEL CONGRESO" />
                    </div>
                </div>
            </TabPanel>
            <TabPanel>
                <div className={tabPanelClass}>
                    <div className={gridVideosClass}>
                        <div className={firstColumnClass}>
                            <h4>Enseñar a aprender, aprender a enseñar</h4>
                            <ButtonLink link="/pdf/programa_enap_2012.pdf">Programa 2012 PDF</ButtonLink>
                            <ButtonLink link="https://www.youtube.com/playlist?list=PLtDwr4xyHdryP4rOlXorBuvnhVXgBMCsz">Vídeos 2012</ButtonLink>
                        </div>
                        <VideoBlock video="https://www.youtube.com/embed/fEZJcVfi-Ew" title="PONENCIA DESTACADA - MADRE MONTSERRAT DEL POZO" />
                        <VideoBlock video="https://www.youtube.com/embed/Ba7d51DzghE" title="PONENCIA DESTACADA - ELAINE GALLAGHER" />
                        <VideoBlock video="https://www.youtube.com/embed/oKAEVB8Sgtc" title="RESUMEN DEL CONGRESO" />
                    </div>
                </div>
            </TabPanel>
        </Tabs>
    )
}

export default LastYears;