import React, { useRef, useEffect, useState } from 'react'

export const AnimatedBackground = (props) => {

    let canvasRef = useRef(null)
    let [size, setSize] = useState({ w: 100, h: 200 })

    let canvas;
    let context;


    let randomColor = () => {
        const color1 = [103, 162, 140];
        const color2 = [224, 214, 145];
        const color3 = [73, 81, 102];
        const colors = [color1, color2, color3];

        let selectedColor = colors[Math.floor(Math.random() * colors.length)];
        let opcacity = 0.1 + Math.random() * 0.6;

        return `rgba(${selectedColor[0]}, ${selectedColor[1]}, ${selectedColor[2]}, ${opcacity})`
    }

    let figures = [];

    const draw = (ctx, frameCount, fps) => {
        //console.log({ ctx, frameCount, fps })
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)

        figures.map(figure => {
            ctx.fillStyle = figure.color;
            ctx.beginPath()
            // ctx.arc(figure.x, figure.y, 20 * Math.sin(frameCount * 0.05) ** 2, 0, 2 * Math.PI)
            ctx.arc(figure.x, figure.y, figure.radio, 0, 2 * Math.PI)//  20 * Math.sin(frameCount * 0.05) ** 2, 0, 2 * Math.PI)
            ctx.fill()
            // figure.y -= 1 / fps * figure.vy;
            if (fps > 0) {
                figure.y -= 1 * (fps / 60) * figure.vy * 0.1;
                figure.x += Math.sin((frameCount + figure.gap) * (figure.xvar / 1000)) * 0.1;
            }
            // console.log(figure);

            if (figure.y < -figure.radio) {
                figure.y = ctx.canvas.height + figure.radio;
            }
        })
    }

    useEffect(() => {
        const canvas = canvasRef.current
        const context = canvas.getContext('2d')
        let frameCount = 0
        let animationFrameId;
        let prevTimestamp;
        // setSize([context.canvas.clientWidth, context.canvas.clientHeight])

        figures = Array(20).fill({ x: 0, y: 0, color: 'black', vy: 1, vx: 0, gap: 0, xvar: 0, radio: 20 });
        console.log(figures)
        figures = figures.map(f => {
            f = { ...f }
            f.x = Math.random() * (context.canvas.width / 2);
            f.y = Math.random() * context.canvas.height;
            f.color = randomColor();
            f.vy = Math.random() + 0.2;
            f.radio = Math.random() * 20 + 5;
            f.gap = Math.floor(Math.random() * 100);
            f.xvar = 10 + Math.floor(Math.random() * 50)
            return f;
        })
        console.log(figures)

        //Our draw came here
        const render = (timestamp) => {
            let fps = 1000 / (timestamp - prevTimestamp);
            prevTimestamp = timestamp
            frameCount++
            draw(context, frameCount, fps)
            animationFrameId = window.requestAnimationFrame(render)
        }
        render()

        return () => {
            window.cancelAnimationFrame(animationFrameId)
        }
    }, [draw])

    useEffect(() => {
        const canvas = canvasRef.current
        const context = canvas.getContext('2d')
        const { width, height } = canvas.getBoundingClientRect();
        setSize({ w: width, h: height })
    }, [])

    return (
        <div className={props.className}>
            <canvas className="w-full h-full max-w-full max-h-full bg-transparent"
                width={size?.w} height={size?.h}
                ref={canvasRef}></canvas>
        </div>
    )
}
