var FtpDeploy = require("ftp-deploy");
var ftpDeploy = new FtpDeploy();
var path = require('path');

var config = {
    user: "ensenara",
    // Password optional, prompted if none given
    password: "a7Y88ygOv7",
    host: "51.254.53.96",
    port: 21,
    localRoot: path.resolve(__dirname, `../dist`),
    remoteRoot: '/public_html/ensenaraaprender.es',
    include: ["*", "**/*"],      // this would upload everything except dot files
    // include: ["*.php", "dist/*", ".*"],
    // e.g. exclude sourcemaps, and ALL files in node_modules (including dot files)
    exclude: ["dist/**/*.map", "node_modules/**", "node_modules/**/.*", ".git/**"],
    // delete ALL existing files at destination before uploading, if true
    deleteRemote: false,
    // Passive mode is forced (EPSV command is not sent)
    forcePasv: true,
    // use sftp or ftp
    sftp: false
};

ftpDeploy
    .deploy(config)
    .then(res => console.log("finished:", res))
    .catch(err => console.log(err));