let axios = require('axios').default;
let fs = require('fs');
let path = require('path');

let relativeDir = '/assets/dimages/'
let targetDir = `../public${relativeDir}`;

let main = async () => {
    // Fetch companies images
    let response = await axios.get('https://dapi.escuelaideo.edu.es/enap/items/companies?fields=name,organizador,logo.filename_disk,logo.private_hash,website');

    let jsonResponse = response.data;
    let companies = jsonResponse.data;

    await Promise.all(companies.map(async (company) => {
        let imageUrl = `https://dapi.escuelaideo.edu.es/enap/assets/${company.logo.private_hash}?key=companies`;

        let response = await axios({
            method: 'get',
            url: imageUrl,
            responseType: 'stream'
        })

        let imageName = `company-${company.logo.filename_disk}`;
        let targetPath = path.resolve(__dirname, `${targetDir}/${imageName}`);
        console.log(targetPath);
        await response.data.pipe(fs.createWriteStream(targetPath));
        console.log(`Got image ${imageName}`);
        company.image = `${relativeDir}${imageName}`;
    }));

    let dataFile = path.resolve(__dirname, `../src/data/companies.json`);
    fs.writeFileSync(dataFile, JSON.stringify(companies));
}

(async () => {
    console.log("Starting");
    await main();
})();