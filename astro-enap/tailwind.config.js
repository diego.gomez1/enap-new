// tailwind.config.js
module.exports = {
  mode: 'jit',
  purge: ['./public/**/*.html', './src/**/*.{astro,js,jsx,ts,tsx,vue}'],
  // more options here
  theme: {
    fontFamily: {
      Poppins: ["'Poppins'", "sans-serif"]
    },
    extend: {
      colors: {
        white: '#ffffff',
        black: '#000000',
        bg1: 'var(--color-bg-1)',
        bg2: 'var(--color-bg-2)',
        enap1: 'var(--color-enap-1)',
        enap2: 'var(--color-enap-2)',
        enap3: 'var(--color-enap-3)',
        enap4: 'var(--color-enap-4)',
        enap5: 'var(--color-enap-5)'
      },
      gridTemplateColumns: {
        footer: '1fr 6em'
      }
    }

  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/aspect-ratio'),
  ]
};
