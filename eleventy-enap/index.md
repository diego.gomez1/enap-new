---
layout: mainlayout.njk
---

# Bienvenidos

## Congreso de innovación educativa

Con mucha ilusión estamos ya preparando el Congreso presencial ENAP 2021, reserva ya estas fechas porque no te lo puedes perder, ¡¡volvemos con más ilusión que nunca!!

En Madrid 22-23 de octubre de 2021

TEMA: Espacios y tiempos..., con otra mirada

En el 2020, el IX congreso ENAP, no pudo ser por el advenimiento pandémico.
Lo planeamos, lo aplazamos, lo imaginamos, lo decidimos hacer presencial y como no fue posible…, nos resistimos a decir que no lo hicimos…, porque reflexionamos, sufrimos, flexibilizamos, entendimos que habíamos cubierto una etapa, un ciclo, …por ello queremos dejar ese hueco, simplemente ENAP 2020 existió, (vaya si existió)...pero se llamó “No nos vimos”.

Ahora en el ENAP 2021, la propuesta consiste en realizar un ENAP diferente, pero no menor, no de menos impacto, no menos profundo.

¡¡Os esperamos!!
