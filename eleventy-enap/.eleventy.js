const pluginSass = require("eleventy-plugin-sass");
let sassPluginOptions = {};
module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(pluginSass, sassPluginOptions);
};
